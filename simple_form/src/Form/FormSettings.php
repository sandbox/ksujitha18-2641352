<?php
/**
 * Simple_ Form
 *
 * PHP version 5
 *
 * @category Example
 * @package  Form
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://www.drupal.org/project/2634676
 */
namespace Drupal\simple_form\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;


/**
 * Simple_ Form
 *
 * @category Example
 * @package  Form
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://www.drupal.org/project/2634676
 */

class FormSettings implements FormInterface
{
    /**
     * Returns a unique string identifying the form.
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId() 
    {
        return 'simple_form';
    }

    /**
     * Form constructor
     *
     * @param array $form       ...containing the structure of the form
     * @param array $form_state ...The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state) 
    {
        $form['firstName'] = array(
        '#type' => 'textfield',
        '#title' => t('First Name'),
        '#length' => 50,
        '#required' => true, 
        ); 

        $form['lastName'] = array(
        '#type' => 'textfield',
        '#title' => t('Last Name'),
        '#length' => 50,
        ); 
 
        $form['emailId'] = array(
        '#type' => 'textfield',
        '#title' => t('Email Id'),
        '#length' => 50,
        '#required' => true, 
        ); 
 
 
        $form['pass'] = array(
        '#type' => 'password_confirm',
        '#length' => 50,
        '#required' => true, 
        );  
    
     
        $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
        ); 
 
        return $form;
      
    }

    /**
     * Form constructor
     *
     * @param array $form       ...containing the structure of the form
     * @param array $form_state ...The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function validateForm(array &$form, FormStateInterface $form_state) 
    {
             
        if (!valid_email_address($form_state->getValue('emailId'))) {
            $form_state->setErrorByName('emailId', t('E-mail address is not valid'));
        }
    
    }

    /**
     * Form constructor
     *
     * @param array $form       ...containing the structure of the form
     * @param array $form_state ...The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        // Save the submitted entry.
        $value = array(
        'firstName'=>$form_state->getValue('firstName'),
        'lastName'=>$form_state->getValue('lastName'),
        'emailId'=>$form_state->getValue('emailId'),
        'pass'=>$form_state->getValue('pass'),
    
        );
    
        $return = FormStorage::insert($value);
        if ($return) {
            drupal_set_message(t('Your Account Successfully Created'));
        }
        

    
    }     
}