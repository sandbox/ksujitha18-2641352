<?php
/**
 * Simple_ Form
 *
 * PHP version 5
 *
 * @category Example
 * @package  Form
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://www.drupal.org/project/2634676
 */

namespace Drupal\simple_form\Form;

/**
 * Simple_ Form
 *
 * @category Example
 * @package  Form
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://www.drupal.org/project/2634676
 */

class FormStorage
{

    /**
     * Form constructor
     *
     * @param array $value ...containing values of form fields
     *
     * @return array
     *   The form structure.
     */
    public static function insert($value) 
    {
        $return_value = null;
        try {
            $return_value = db_insert('simple_form')
            ->fields($value)
            ->execute();
        }
        catch (\Exception $e) {
            drupal_set_message(
                t(
                    'db_insert failed. Message = %message, query= %query', array(
                    '%message' => $e->getMessage(),
                    '%query' => $e->query_string,
                    )
                ), 'error'
            );
        }
        return $return_value;
    }

  
}
